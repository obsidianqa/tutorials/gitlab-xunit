using Xunit;
using Program;
    
namespace program.tests
{
    public class CalculatorTests
    {
        public class Add
        {
            [Fact]
            public void OnePlusOne_ReturnsTwo()
            {
                var calculator = new Calculator();
                
                var expected = 2;
                var actual = calculator.Add(1, 1);
                
                Assert.Equal(expected, actual);
            }
        }
    }
}
